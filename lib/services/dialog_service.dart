import 'package:flutter/material.dart';

/// This service makes it easy to manage your dialogs
class DialogService {
  static final DialogService instance = DialogService._();

  GlobalKey<NavigatorState>? navigatorKey;
  IDialog? currentDialog;

  DialogService._();

  void initialize(GlobalKey<NavigatorState> navigatorKey) {
    this.navigatorKey = navigatorKey;
  }

  Future<void> show(IDialog dialog) async {
    _check();

    if (currentDialog != null) {
      throw "[DialogService] last showed 'IDialog' with type - '${currentDialog.runtimeType}' wasn't closed. "
          "Please close current 'IDialog' before showing any 'IDialog'";
    }
    currentDialog = dialog;

    await showDialog(
      context: navigatorKey!.currentState!.context,
      builder: dialog.build,
      barrierColor: dialog.settings.barrierColor,
      barrierDismissible: dialog.settings.barrierDismissible,
      barrierLabel: dialog.settings.barrierLabel,
      useSafeArea: dialog.settings.useSafeArea,
      useRootNavigator: dialog.settings.useRootNavigator,
      routeSettings: dialog.settings.routeSettings,
    ).then((value) => currentDialog = null);

  }

  void close() {
    _check();

    if (currentDialog == null) {
      return;
    }

    navigatorKey!.currentState!.pop();

    currentDialog = null;
  }

  void _check() {
    if (navigatorKey == null) {
      throw "Navigator key was null. Did you call 'DialogService.initialize(...)'?";
    }
    if (navigatorKey!.currentState == null) {
      throw "[DialogService] 'currentState' property of 'navigatorKey' field was null. Did you put this 'navigatorKey' to 'navigatorKey' property in 'MaterialApp' widget?";
    }
  }
}

/// This class is a bunch of properties for [showDialog] function
class DialogSettings {
  final bool barrierDismissible;
  final Color? barrierColor;
  final String? barrierLabel;
  final bool useSafeArea;
  final bool useRootNavigator;
  final RouteSettings? routeSettings;

  DialogSettings({
    this.useSafeArea = true,
    this.useRootNavigator = true,
    this.barrierDismissible = true,
    this.barrierColor,
    this.barrierLabel,
    this.routeSettings,
  });

  static DialogSettings defaultSettings = DialogSettings(
    barrierDismissible: true,
    useSafeArea: true,
    useRootNavigator: true,
    barrierColor: Colors.black54,
  );

  static DialogSettings irremovableSettings = DialogSettings(
    barrierDismissible: false,
    useSafeArea: true,
    useRootNavigator: true,
    barrierColor: Colors.black54,
  );

  static DialogSettings unsafeAreaSettings = DialogSettings(
    barrierDismissible: true,
    useSafeArea: false,
    useRootNavigator: true,
    barrierColor: Colors.black54,
  );

  static DialogSettings unsafeAreaIrremovableSettings = DialogSettings(
    barrierDismissible: false,
    useSafeArea: false,
    useRootNavigator: true,
    barrierColor: Colors.black54,
  );
}

/// This interface provides you with using your widget in [DialogService]
/// by overriding [DialogSettings] property.
abstract class IDialog {
  DialogSettings get settings;

  Widget build(BuildContext context);
}
