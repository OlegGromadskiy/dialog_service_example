import 'package:dialog_manager/services/dialog_service.dart';
import 'package:flutter/material.dart';

final key = GlobalKey<NavigatorState>();

void main() {
  DialogService.instance.initialize(key);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: key,
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextButton(
                onPressed: () => DialogService.instance.show(DefaultDialog()),
                child: Text('default dialog'),
              ),
              TextButton(
                onPressed: () => DialogService.instance.show(IrremovableDialog()),
                child: Text('irremovable dialog'),
              ),
              TextButton(
                onPressed: () => DialogService.instance.show(CustomDialog()),
                child: Text('custom dialog'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DefaultDialog extends StatelessWidget implements IDialog {
  @override
  DialogSettings get settings => DialogSettings.defaultSettings;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Center(
        child: Material(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 200.0,
            child: Column(
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    onPressed: () => DialogService.instance.close(),
                    icon: Icon(Icons.close),
                  ),
                ),
                Text('Default dialog'),
                const SizedBox(height: 24.0),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class IrremovableDialog extends StatelessWidget implements IDialog {
  @override
  DialogSettings get settings => DialogSettings.irremovableSettings;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Center(
        child: Material(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 200.0,
            child: Column(
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    onPressed: () => DialogService.instance.close(),
                    icon: Icon(Icons.close),
                  ),
                ),
                Text('Irremovable dialog'),
                const SizedBox(height: 24.0),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CustomDialog extends StatelessWidget implements IDialog {
  @override
  DialogSettings get settings => DialogSettings(
        barrierColor: Colors.pink.withOpacity(0.3),
        barrierDismissible: false,
      );

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Center(
        child: Material(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 200.0,
            child: Column(
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    onPressed: () => DialogService.instance.close(),
                    icon: Icon(Icons.close),
                  ),
                ),
                Text('Custom dialog'),
                const SizedBox(height: 24.0),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
